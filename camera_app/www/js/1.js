document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    window.plugins.googleplus.isAvailable(
        function (available) {
            if (available) {
                alert("Login available");
                document.getElementById("login").addEventListener("click", login);
            }
        }
    );
}

function login() {
    alert("logging in");
    window.plugins.googleplus.login({
            'scopes': '... ', // optional space-separated list of scopes, the default is sufficient for login and basic profile info 
            // there is no API key for Android; you app is wired to the Google+ API by listing your package name in the google dev console and signing your apk (which you have done in chapter 4) 
        },
        function (obj) {
            alert(JSON.stringify(obj));
            // do something useful instead of alerting 
        },
        function (msg) {
            alert('error: ' + msg);
        }
    );

}


//
//function onSuccess(imageData)
//{
//}
//function onFail(message)
//{alert('Failed because:'+ message);}