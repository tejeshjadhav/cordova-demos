var createStatement = "CREATE TABLE IF NOT EXISTS data1 (id INTEGER PRIMARY KEY AUTOINCREMENT, data_1 TEXT, data_2 TEXT)";

var selectAllStatement = "SELECT * FROM data1";

var insertStatement = "INSERT INTO data1 (data_1, data_2) VALUES (?, ?)";

var db = openDatabase("data_table", "1.0", "Book", 200000);  // Open SQLite Database

var dataset;
 
var DataType;


function createTable()  // Function for Create Table in SQLite.
 
{
 
    db.transaction(function (tx) { tx.executeSql(createStatement, [], onError); });
 
}

function insertRecord() // Get value from Input and insert record . Function Call when Save/Submit Button Click..
 
{
 
        var data_1temp = $('input:text[id=id1]').val().toString();
 
        var data_2temp = $('input:text[id=id2]').val().toString();
        db.transaction(function (tx) { tx.executeSql(insertStatement, [data_1temp, data_2temp], ); });
 
        //tx.executeSql(SQL Query Statement,[ Parameters ] , Success Result Handler Function, Error Result Handler Function );
		
 
}


function onError() // Function for Handling Error...
 
{
 
    alert("error");
 
}



$(document).ready(function () // Call function when page is ready for load..
 
{
;
 
    $("body").fadeIn(20000); // Fade In Effect when Page Load..
 
    $("#b1").click(insertRecord);  // Register Event Listener when button click.

 
});